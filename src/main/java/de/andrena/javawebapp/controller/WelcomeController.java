package de.andrena.javawebapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;

@Controller
public class WelcomeController {

    @GetMapping("/")
    public String main(Model model) {
        model.addAttribute("message", "Welcome to your fancy Java Web App");
        model.addAttribute("learnedThings", Arrays.asList("Create container from base container", "Add file to container", "Set Execute Command of container"));

        return "welcome";
    }

    @GetMapping("/crash")
    public String crash(Model model) {
        System.exit(1);
        return "crashed";
    }
}
